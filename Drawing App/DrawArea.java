import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;

/** compontas for drawuing */
public class DrawArea extends JComponent {
    private static final long serialVersionUID = 1L;

    private Image image;
    private Graphics2D g2;
    private int oldX, oldY, currantX, currantY;

    public DrawArea() {
        setDoubleBuffered(false);
        addMouseListener(new MouseAdapter(){

            public void mousePressed(MouseEvent e){
                oldX=e.getX();
                oldY=e.getY();
            }


        
        });

        addMouseMotionListener(new MouseMotionAdapter(){
            public void mouseDragged(MouseEvent e){
                currantX=e.getX();
                currantY=e.getY();

                if(g2!=null){
                    g2.drawLine(oldX, oldY,currantX,currantY);
                    repaint();
                    oldX=currantX;
                    oldY=currantY;
                }
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
       if(image==null){
           image=createImage(getSize().width,getSize().height);
           g2=(Graphics2D)image.getGraphics();
           g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
           clear();
       }
       g.drawImage(image, 0, 0, null);
    }

    public void clear(){
        g2.setPaint(Color.white);
        g2.fillRect(0, 0,getSize().width,getSize().height);
        g2.setPaint(Color.black);
        repaint();
    }

    public void red(){
        g2.setPaint(Color.red);
    }
    public void blue(){
        g2.setPaint(Color.blue);
    }
    public void green(){
        g2.setPaint(Color.green);
    }
    public void black(){
        g2.setPaint(Color.black);
    }
    public void magenta(){
        g2.setPaint(Color.magenta);
    }
}