package renderer;

import java.util.Hashtable;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.BorderFactory;


import renderer.point.*;
import renderer.shapes.*;


public class Display extends Canvas implements Runnable{

    private static final long serialVersionUID = 1L;

    private Thread thread;// new thread as every verion will run on its own thread 
    private JFrame frame; // creates a new Jframe to draw on 
    private static String title="3D render"; //screen title
    private static boolean isRunning=false; //reperesnt the state of the renderer if it is running or not
    private Tetrahedron cube;
    //private Tetrahedron pyramid;
    private JSlider rotateX;
    private JSlider rotateY;
    private JSlider rotateZ;
    private JSlider scale;

    public static final int WIDTH=800; //fixed veribles of width of the screen
    public static final int HEIGHT=600; //fixed veribles of hight of the screen 
    public Hashtable<Integer, JLabel> labels = new Hashtable<>();
    public Hashtable<Integer, JLabel> l = new Hashtable<>();

    //crteas the windown given the dimentions of hight and whith 
    public Display(){

        Dimension size =new Dimension(WIDTH,HEIGHT); 
        this.setPreferredSize(size);
    }

    public static void main(String[] args){
        addUI();
    }

    //add all UI componats to JFrame
    public static void addUI(){
        Display display =new Display();
        display.frame=new JFrame();
        display.addRotationSliders();
        display.addScaleSlider();
        display.frame.setTitle(title);
        Container mainContainer=display.frame.getContentPane();
        mainContainer.setLayout(new BorderLayout(8,6));
        display.frame.getRootPane().setBorder(BorderFactory.createMatteBorder(4,4,4,4,Color.BLACK));

        //top Panel.
        JPanel topPanel=new JPanel();
        topPanel.setBorder(new LineBorder(Color.BLACK,3));
        topPanel.setBackground(Color.DARK_GRAY);
        topPanel.setLayout(new FlowLayout(5));
        mainContainer.add(topPanel,BorderLayout.NORTH);

        //middle panel.
        JPanel middlePanel=new JPanel();
        middlePanel.setBorder(new LineBorder(Color.BLACK,3));
        middlePanel.setBackground(Color.GRAY);
        middlePanel.setLayout(new FlowLayout(4,4,4));


        JPanel gridPanel= new JPanel();
        gridPanel.setLayout(new GridLayout(1,1,5,5));
        gridPanel.setBorder(new LineBorder(Color.BLACK,3));
        gridPanel.add(display.scale);


        middlePanel.add(gridPanel);
        mainContainer.add(display);
        mainContainer.add(middlePanel, BorderLayout.WEST);

        //bottom Panel.
        JPanel bottomPanel=new JPanel();
        bottomPanel.setLayout(new FlowLayout(3));

        bottomPanel.add(display.rotateX);
        bottomPanel.add(display.rotateY);
        bottomPanel.add(display.rotateZ);

        bottomPanel.setBackground(Color.DARK_GRAY);
        bottomPanel.setBorder(new LineBorder(Color.BLACK,3));
        mainContainer.add(bottomPanel,BorderLayout.SOUTH);

        //add to screen
        display.frame.pack();
        display.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        display.frame.setLocationRelativeTo(null);
        display.frame.setResizable(true);
        display.frame.setVisible(true);
        display.Start();
    }

    private void addRotationSliders(){
               
        addRotationLables();

        rotateX = new JSlider(SwingConstants.HORIZONTAL, 0, 360, 0);
        rotateX.setMajorTickSpacing(20); //will set tick marks every 10 pixels
        rotateX.setPaintTicks(true); //this actually paints the ticks on the screen
        rotateX.setBorder(BorderFactory.createTitledBorder("X axsis Rotation"));

        rotateX.setLabelTable(labels);
        rotateX.setPaintLabels(true);

        rotateY = new JSlider(SwingConstants.HORIZONTAL, 0, 360, 45);
        rotateY.setMajorTickSpacing(20); //will set tick marks every 10 pixels
        rotateY.setPaintTicks(true); //this actually paints the ticks on the screen
        rotateY.setBorder(BorderFactory.createTitledBorder("Y axsis Rotation"));

        rotateY.setLabelTable(labels);
        rotateY.setPaintLabels(true);

        rotateZ = new JSlider(SwingConstants.HORIZONTAL, 0, 360, 45);
        rotateZ.setMajorTickSpacing(20); //will set tick marks every 10 pixels
        rotateZ.setPaintTicks(true); //this actually paints the ticks on the screen
        rotateZ.setBorder(BorderFactory.createTitledBorder("Z axsis Rotation"));

        rotateZ.setLabelTable(labels);
        rotateZ.setPaintLabels(true);

    }
    
    public void addScaleSlider(){
        scale = new JSlider(SwingConstants.VERTICAL, 0, 200, 100);
        scale.setMajorTickSpacing(20); //will set tick marks every 10 pixels
        scale.setPaintTicks(true); //this actually paints the ticks on the screen
        scale.setBorder(BorderFactory.createTitledBorder("scale"));

        l.put(0, new JLabel("0 \u0025"));
        l.put(50, new JLabel("50 \u0025"));
        l.put(100, new JLabel("100 \u0025"));
        l.put(150, new JLabel("150 \u0025"));
        l.put(200, new JLabel("200 \u0025"));

        scale.setLabelTable(l);
        scale.setPaintLabels(true);
    }

    //add lables to sliders
    private void addRotationLables(){

        labels.put(0, new JLabel("0 \u00B0"));
        labels.put(90, new JLabel("90 \u00B0"));
        labels.put(180, new JLabel("180 \u00B0"));
        labels.put(270, new JLabel("270 \u00B0"));
        labels.put(360, new JLabel("360 \u00B0"));

    }


    public synchronized void Start(){
        isRunning=true;
        this.thread=new Thread(this,"Display");
        this.thread.start();


    }

    public synchronized void Stop(){
        try{
            isRunning=false;
            this.thread.join();
        }catch(Exception e){
            System.out.println("an error occoured:");
            System.out.println(e);
            e.printStackTrace();
        }
  
        
    }

    @Override
    public void run() {
        long timeLasting=System.nanoTime(); //used to record what time it was during last itterastion of while loop
        long timer=System.currentTimeMillis(); //used to recrod what the time was during currnt itterstion of loop
        final double ns=1000000000.0/60; // ns will repsent the number of nanao seconds between each update provided  updatiung at 60 updates per second
        double delta=0; // reprsent progress to next update
        int frames =0; //frame tracker


        while(isRunning){
            init();
            long now = System.nanoTime();
            delta+=(now-timeLasting)/ns;
            timeLasting=now;
            //a while loop is used to prevent the computer from skipping an update
            while(delta>=1){
                update();
                delta--;
                render();
                frames++;
            }
            if(System.currentTimeMillis()-timer>1000){
                timer+=1000;
                this.frame.setTitle(title+ ": "+ frames + " FPS");
                frames=0;
            }
        }
        Stop();
    }

    public void addLayout(){
        //TODO ADD STUFF BACKONCE MOUSE IS WROTE
    }


    //draw the  polygon at evry instace rather than ever frame.
    private void init(){
        int s=scale.getValue(); //scale of camera.
        MyPoint p1= new MyPoint(s/2,-s/2,-s/2);
        MyPoint p2= new MyPoint(s/2,s/2,-s/2);
        MyPoint p3= new MyPoint(s/2,s/2,s/2);
        MyPoint p4= new MyPoint(s/2,-s/2,s/2);
        MyPoint p5= new MyPoint(-s/2,-s/2,-s/2);
        MyPoint p6= new MyPoint(-s/2,s/2,-s/2);
        MyPoint p7= new MyPoint(-s/2,s/2,s/2);
        MyPoint p8= new MyPoint(-s/2,-s/2,s/2);

        this.cube=new Tetrahedron(
                    new MyPolygon(Color.RED,p1,p2,p3,p4), //front face. 
                    new MyPolygon(Color.BLUE,p5,p6,p7,p8), //back face.
                    new MyPolygon(Color.GREEN,p1,p2,p6,p5),//top face.
                    new MyPolygon(Color.ORANGE,p1,p5,p8,p4),//right face.
                    new MyPolygon(Color.YELLOW,p2,p6,p7,p3),//left face.
                    new MyPolygon(Color.WHITE,p4,p3,p7,p8));//bottom face.
        
        //thechicaly a pyrmaid, just not a reguler one.
       /* this.pyramid=new Tetrahedron(
                        new MyPolygon(Color.RED,p1,p2,p3), //front face 
                        new MyPolygon(Color.BLUE,p1,p4,p5), //back face
                        new MyPolygon(Color.ORANGE,p1,p2,p4),//right face
                        new MyPolygon(Color.YELLOW,p1,p3,p5),//left face
                        new MyPolygon(Color.WHITE,p2,p3,p4,p5));//bottom face.

        .*/
                        
        
        
    }


    private void render() {
        BufferStrategy bs=this.getBufferStrategy();
        // if there is no bufferdstaragey cretae and return
        if(bs==null){
            this.createBufferStrategy(3);
            return;
        }
         
        Graphics g= bs.getDrawGraphics();
        g.setColor(Color.black);
        g.fillRect(0, 0, WIDTH, HEIGHT);

        cube.render(g);
        
        g.dispose();
        bs.show();

    }

    private void update(){
        this.cube.rotate(false,0,0,1);
    }


}