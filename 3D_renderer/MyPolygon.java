package renderer.shapes;

import renderer.point.MyPoint;
import renderer.point.PointConverter;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.util.*;
import java.awt.Point;

public class MyPolygon {
    
    private Color color;
    private MyPoint[] points;

    /**
     * the ... notation is to signify that you can send anysize arry in java 
     * this method will cretae a deep copy of all the poionts given.
     * 
     */
    public MyPolygon(Color color,MyPoint... points){
        this.color=color;
        this.points=new MyPoint[points.length];
        for(int i=0;i<points.length;i++){
            MyPoint p= points[i];
            this.points[i]=new MyPoint(p.x, p.y, p.z);
        }
    }

    public MyPolygon(MyPoint... points){
        this.color=Color.BLUE;
        this.points=new MyPoint[points.length];
        for(int i=0;i<points.length;i++){
            MyPoint p= points[i];
            this.points[i]=new MyPoint(p.x, p.y, p.z);
        }
    }


    public void render(Graphics g){
        Polygon poly=new Polygon();
        //convert all poins in 3d shape to 2d
        for(int i=0;i<this.points.length;i++){
            Point p =PointConverter.convertPoint(this.points[i]);
            poly.addPoint(p.x, p.y);
        }
        g.setColor(this.color);
        g.fillPolygon(poly);
    }

    public void rotate(boolean clockWise,double xDegrees,double yDegrees,double zDegrees){
        for(MyPoint p: points){
            PointConverter.rotateAxisX(p,clockWise,xDegrees);
            PointConverter.rotateAxisY(p,clockWise,yDegrees);
            PointConverter.rotateAxisZ(p,clockWise,zDegrees);
        }
    }

    //averge out dsitace between points, this may need optimising later
    public double getAverageX(){
        double sum=0;
        for(MyPoint p: this.points){
            sum+=p.x;
        }
        return sum/this.points.length;
    }

    public void setColor(Color color){
        this.color=color;
    }

    public static MyPolygon[] sortPolygons(MyPolygon[] polygons){
        List<MyPolygon> polygonList=new ArrayList<MyPolygon>();

        //add all points to a linked list
        for(MyPolygon poly: polygons){
            polygonList.add(poly);
        }

        //sort the vlaues so that the furtest point away is drawn first
        Collections.sort(polygonList,new Comparator<MyPolygon>() {
            @Override
            public int compare(MyPolygon p1,MyPolygon p2){
                return p2.getAverageX() - p1.getAverageX() <0 ?1:-1;
            }
        });
        for(int i=0;i<polygons.length;i++){
            polygons[i]=polygonList.get(i);
        }

        return polygons;
    }

 
}