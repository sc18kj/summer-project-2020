package renderer.point;

import java.awt.Point;
import renderer.Display;
public class PointConverter {
    
    private static double scale=1;
    // a computer is unable to repersent 3d space in 2d so this mthod is to convert the 3d point to 2d
    public static Point convertPoint(MyPoint point3D){
        //normaises the values so x=x,y=y,z=z
        double x3D=point3D.y; 
        double y3D=point3D.z; 
        double depth= point3D.x *scale;
        double[] newVal=scale(x3D,y3D,depth);
        int x2D=(int)(Display.WIDTH/2+newVal[0]);
        int y2D=(int) (Display.HEIGHT/2-newVal[1]); 

        Point Point2D=new Point(x2D,y2D);
        return Point2D;
    }

    //adds a focus poins so the user can see the 3D space
    private static double[] scale(double x3D,double y3D,double depth){
        //uses diustace forual (the square root of sum of sqaures) inogre x cornafte and get distace on the vector of y and z before truakting it
        double dist=Math.sqrt(x3D*x3D+y3D*y3D);
        double depth2=15-depth; // depth relative to camera
        double theta=Math.atan2(y3D,x3D); //angle of vecotr workd out by trig
        double localScale=Math.abs(1400/(depth2 + 1400));// scale is = to n/n-depth2 where n is a fixed value (in this case 1400)
        dist *= localScale;

        double[] newVal= new double[2];
        newVal[0]=dist * Math.cos(theta);
        newVal[1]=dist * Math.sin(theta);

        return newVal;
    }

    public static void rotateAxisX(MyPoint p,boolean clockWise,double degrees){
        double radius= Math.sqrt(p.y*p.y + p.z*p.z); //pytahgors therum to give a vector
        double theta=Math.atan2(p.z, p.y);
        theta+=2*Math.PI/360*degrees*(clockWise?-1:1);//convert degrees to radious then multiplay by 1 or -1
        p.z=radius*Math.sin(theta);//basic trig to work out new corndiante vecotr
        p.y=radius*Math.cos(theta);
    }


    public static void rotateAxisY(MyPoint p,boolean clockWise,double degrees){
        double radius= Math.sqrt(p.x*p.x + p.z*p.z); //pytahgors therum to give a vector
        double theta=Math.atan2(p.x, p.z);
        theta+=2*Math.PI/360*degrees*(clockWise?-1:1);//convert degrees to radious then multiplay by 1 or -1
        p.z=radius*Math.cos(theta);//basic trig to work out new corndiante vecotr
        p.x=radius*Math.sin(theta);
    }


    public static void rotateAxisZ(MyPoint p,boolean clockWise,double degrees){
        double radius= Math.sqrt(p.y*p.y + p.x*p.x); //pytahgors therum to give a vector
        double theta=Math.atan2(p.y, p.x);
        theta+=2*Math.PI/360*degrees*(clockWise?-1:1);//convert degrees to radious then multiplay by 1 or -1
        p.x=radius*Math.cos(theta);//basic trig to work out new corndiante vecotr
        p.y=radius*Math.sin(theta);
    }
}