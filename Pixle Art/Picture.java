import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
public class Picture {

    public Picture(int width, int height) {
        BufferedImage img=new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
        File f=null;

        for(int i=0;i<height;i++){
            for(int j=0;j<width;j++){
                int a=(int)(Math.random()*256);
                int r=(int)(Math.random()*256);
                int g=(int)(Math.random()*256);
                int b=(int)(Math.random()*256);

                int pixle=(a<<24)|(r<<16)|(g<<8)|b;
                img.setRGB(j, i, pixle);
            }
        }

        try{
            f=new File("images/Output.png");
            ImageIO.write(img,"png",f);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public static void main(String args[]){
        try{
            new Picture(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        } catch(Exception e){
            System.out.println("no commandline arguments found useing defult paramters of 640 and 320");
            new Picture(640,320);
        }
    }

}