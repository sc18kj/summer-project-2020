import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

public class Level {

    Handler handler;
    Player player;
    BufferedImage image;

    public Level(Handler handler,Player player) {
        this.handler = handler;
        this.player=player;
        try {
            getLevelMap();
            loadLevel(image);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getLevelMap() throws IOException {
        image=ImageIO.read(new File("../assets/levels/level_1.png"));
    }

    //load level
    private void loadLevel(BufferedImage image){
    int w=image.getWidth();
    int h=image.getHeight();
    boolean exitPlaced=false;
    Random r=new Random();
    for(int i=0;i<w;i++){
        for(int j=0;j<h;j++){
            //get the colors to make the level
            int pixel= image.getRGB(i, j);
            int red=(pixel>>16)&0xff;
            int green=(pixel>>8)&0xff;
          //  int blue=(pixel)&0xff;

            if(red>=200){
                handler.addObject(new Wall(i*32,j*32,ID.Wall,false));
            }
            else if(green>=200){
                handler.addObject(new Enemy(i*32, j*32, ID.Enemy, handler, player));
            }
            else{
                if(!exitPlaced && red<199){
                    handler.addObject(new Exit(r.nextInt(w)*32, r.nextInt(h/2)*32,ID.Exit));
                    exitPlaced=true;
                }

            }
           
        }
     }

    }
    

}