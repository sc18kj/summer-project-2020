import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Game extends Canvas implements Runnable {

    // idk wghat this dose it fixes a warning
    private static final long serialVersionUID = 1L;

    // accessorts to other classes that are required
    private Thread thread;
    private boolean running = false;
    private Handler handler;
    private Camera camera;
    private Player player;
    private BufferedImage floor = null;

    // width and height of the screen should never chage
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 650;

    public Game() {
        new Window(WIDTH, HEIGHT, "adventure game", this);
        start();
        handler = new Handler();
        camera = new Camera(0, 0);
        player = new Player(100, 100, ID.Player, handler);
        this.addKeyListener(new KeyInput(handler));
        this.addMouseListener(new MouseInput(handler, camera, player));

        handler.addObject(player);
        new Level(handler,player);

        // loads grass tetxtrue
        try {
            floor = ImageIO.read(new File("../assets/tiles/grass.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) throws Exception {
        new Game();
    }

    @Override
    public void run() {
        long timeLasting = System.nanoTime(); // used to record what time it was during last itterastion of while loop
        long timer = System.currentTimeMillis(); // used to recrod what the time was during currnt itterstion of loop
        final double ns = 1000000000.0 / 60; // ns will repsent the number of nanao seconds between each update provided
                                             // updatiung at 60 updates per second
        double delta = 0; // reprsent progress to next update
        int frames = 0; // frame tracker

        // game loop
        while (running) {
            long now = System.nanoTime();
            delta += (now - timeLasting) / ns;
            timeLasting = now;
            // a while loop is used to prevent the computer from skipping an update
            while (delta >= 1) {
                tick();
                delta--;
            }
            if (running) {
                render();
            }
            frames++;
            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                System.out.println("FPS = " + frames);
                frames = 0;
            }
        }
        stop();

    }

    public synchronized void start() {
        thread = new Thread(this);
        thread.start();
        running = true;
    }

    public synchronized void stop() {
        try {
            thread.join();// this cloases the thread
            running = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void render() {
        BufferStrategy bs = this.getBufferStrategy();
        // if there is no bufferdstaragey cretae and return
        if (bs == null) {
            this.createBufferStrategy(3); // peramiter is numebr of buffers 3 is reocmeneded (cant have less)
            return;
        }

        Graphics g = bs.getDrawGraphics();
        Graphics2D g2d=(Graphics2D)g;

        g2d.translate(-camera.getX(),-camera.getY());


        g.setColor(Color.BLACK);
        g.fillRect(0, 0, WIDTH, HEIGHT);

        //loads grass texture to screen 
        for(int i=0;i<30*72;i+=32){
           for(int j=0;j<30*72;j+=32){
               g.drawImage(floor, i, j, null);
           }
        }

        handler.render(g);// renders all the stuff e.g player
        g2d.translate(camera.getX(),camera.getY());
        new HUD(g, player.hp, player.mp,player.exp);
        g.dispose();
        bs.show();

    }

    public void tick() {
        for(int i=0;i<handler.objects.size();i++){
            if(handler.objects.get(i).getId()==ID.Player){
                camera.tick(handler.objects.get(i));
            }
        }
        handler.tick();
    }


    
}
