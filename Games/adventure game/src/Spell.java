import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Spell extends GameObject {

    private Handler handler;

    public int lifeSpan=20;

    public Spell(int x, int y, ID id, Handler handler,int mx,int my) {
        super(x, y, id);
        this.handler=handler;

        volX=(mx-x)/10; //speed of the bullet
        volY=(my-y)/10;
    }

    @Override
    public void tick() {
       x+=volX;
       y+=volY;
       
       lifeSpan-=1;
       
       for(int i=0;i<handler.objects.size();i++){
        GameObject tempObject=handler.objects.get(i);
        if(tempObject.getId()==ID.Wall){
            if(getBounds().intersects(tempObject.getBounds())){
                handler.removeObject(this);
            }
        }
      }

      if(lifeSpan<=0){
        handler.removeObject(this);
      }
    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.green);
        g.fillOval(x, y, 8, 8);

    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x,y,8,8);
    }
    
}