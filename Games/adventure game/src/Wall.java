import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

public class Wall extends GameObject {

    private boolean moveing=false;
    private BufferedImage texture;


    public Wall(int x, int y, ID id,boolean moveing) {
        super(x, y, id);
        this.moveing=moveing;
        try{
            texture=ImageIO.read(new File("../assets/tiles/brick.png"));
        } catch(Exception e){
            e.printStackTrace();
            System.out.println("Could not load texture");
        }
       

    }


    @Override
    public void tick() {
        if(moveing){
            x+=volX;
            y+=volY;
            if(x>0&&x<700){
                volX=5;
            }
        }
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(texture, x, y, null);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x,y,32,32);
    }
    
}