import java.awt.*;
public class MpPotion extends GameObject{

    public MpPotion(int x, int y, ID id) {
        super(x, y, id);
    }


    @Override
    public void tick() {
     
    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.yellow);
        g.fillRect(x, y, 16, 16);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x,y,32,32);
    }
    

}