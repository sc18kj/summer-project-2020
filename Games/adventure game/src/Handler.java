import java.util.*;
import java.awt.*;

public class Handler {
    LinkedList<GameObject> objects= new LinkedList<GameObject>();

    private boolean up=false,down=false,left=false,right=false; //verivbles used to prevent button lag

    public void tick(){
        for(int i=0;i<objects.size();i++){
            GameObject tempObject=objects.get(i);
            tempObject.tick();
        }
    }

    public void render(Graphics g){
        for(int i=0;i<objects.size();i++){
            GameObject tempObject=objects.get(i);
            tempObject.render(g);
        }
    }

    public void addObject(GameObject tempObject){
        objects.add(tempObject);
    }
    public void removeObject(GameObject tempObject){
        objects.remove(tempObject);
    }


    public LinkedList<GameObject> getObjects() {
        return this.objects;
    }

    public void setObjects(LinkedList<GameObject> objects) {
        this.objects = objects;
    }

    public boolean isUp() {
        return this.up;
    }

    public boolean getUp() {
        return this.up;
    }

    public void setUp(boolean up) {
        this.up = up;
    }

    public boolean isDown() {
        return this.down;
    }

    public boolean getDown() {
        return this.down;
    }

    public void setDown(boolean down) {
        this.down = down;
    }

    public boolean isLeft() {
        return this.left;
    }

    public boolean getLeft() {
        return this.left;
    }

    public void setLeft(boolean left) {
        this.left = left;
    }

    public boolean isRight() {
        return this.right;
    }

    public boolean getRight() {
        return this.right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

}