import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

public class MouseInput extends MouseAdapter {

    private Camera camera;
    private Handler handler;
    private Player player;
    public MouseInput(Handler handler,Camera camera,Player player) {
        this.camera = camera;
        this.player=player;
        this.handler=handler;
    }


    public void mousePressed(MouseEvent e){
        int mx=(int)(e.getX()+camera.getX());
        int my=(int)(e.getY()+camera.getY());

        //left click
        if(SwingUtilities.isLeftMouseButton(e)){
            
        }

        //right click to cast spell
        if(SwingUtilities.isRightMouseButton(e)){
            for(int i=0;i<handler.objects.size();i++){
                GameObject tempObject=handler.objects.get(i);
                if(player.mp>0 && tempObject.getId()==ID.Player){
                   player.mp--;
                   handler.addObject(new Spell(tempObject.getX()+16, tempObject.getY()+24, ID.Spell,handler,mx,my));
                }
            }
        }
       
    }




}