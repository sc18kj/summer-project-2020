import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class Exit extends GameObject {

    public Exit(int x, int y, ID id) {
        super(x, y, id);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        g.setColor(new Color(165,42,42));
        g.fillOval(x, y, 32, 32);

    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x,y,32,32);
    }
    
}