import java.awt.*;
import java.util.Random;

public class Enemy extends GameObject {

    private Handler handler;
    private Player player;
    private Random r = new Random();

    private int hp=100;

    public Enemy(int x, int y, ID id,Handler handler,Player player){
        super(x, y, id);
        this.handler=handler;
        this.player=player;


        volX=0;
        volY=0;
    }

    @Override
    public void tick() {
        collision();
        x+=volX;
        y+=volY;

        if(hp<=0){
            player.exp++;
            handler.removeObject(this);
        }

        volX=(r.nextInt(4- -4)+ -4);
        volY=(r.nextInt(4- -4)+ -4);
    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.RED);
        g.fillRect(x, y, 32, 32);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x,y,32,32);
    }

    public Rectangle getBoundsBig() {
        return new Rectangle(x,y,64,64);
    }


    private void collision() {
        for(int i=0;i<handler.objects.size();i++){
            GameObject tempObject=handler.objects.get(i);
            if(tempObject.getId()==ID.Wall){
                if(getBoundsBig().intersects(tempObject.getBounds())){
                    x+=volX*-1;
                    y+=volY*-1;
                    volX *= -1;
                    volY *= -1;
                }
            }
            
            if(tempObject.getId()==ID.Spell){
                if(getBounds().intersects(tempObject.getBounds())){
                    hp-=5;
                }
            }

        
        }
    }
  
    
}