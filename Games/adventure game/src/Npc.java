import java.awt.*;

public class Npc extends GameObject {
    

    public Npc(int x, int y, ID id) {
        super(x, y, id);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.GREEN);
        g.fillRect(x, y, 32, 48);

    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x,y,32,48);
    }

    public Rectangle getBoundsBig() {
        return new Rectangle(x,y,64,48*2);
    }



}