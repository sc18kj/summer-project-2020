public enum ID {
    Player(),
    Wall(),
    Enemy(),
    Spell(),
    NPC(),
    MpPotion(),  
    Exit();
}