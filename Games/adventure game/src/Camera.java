
//use Graphics2D maethod to traslate rendieng and follow it
public class Camera {
    private float x,y;

    public Camera(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void tick(GameObject object){
        x+=((object.getX()-x)-1080/2)*0.05f; //tweeing algrothim to fix onto player
        y+=((object.getY()-y)-650/2)*0.05f;

        //clamp the camera so it stays inbounds 
        
        if(x<=0){
            x=0;
        }

        if(x>=1080){
            x=1080;
        }

        if(y<=0){
            y=0;
        }

        if(y>=650){
            y=650;
        }
    }
    

    public float getX() {
        return this.x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return this.y;
    }

    public void setY(float y) {
        this.y = y;
    }


}