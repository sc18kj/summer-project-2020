import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Color;

public class Player extends GameObject {

    private Handler handler;

    // veribles
    public int hp = 100;
    public int mp = 50;
    public int exp = 0;
    public int level = 1;


    public Player(int x, int y, ID id, Handler handler) {
        super(x, y, id);
        this.handler=handler;

    }

    @Override
    public void tick() {
        x+=volX;
        y+=volY;

        collision();
        checkLevelUp();
        //check if the key is pressed then set the veribles and through the platyer class chekcing if the verible is ture and react acroingly

        if(handler.isUp()){
            volY=-5;
        }
        else if(!handler.isDown()){
            volY=0;
        }

        if(handler.isDown()){
            volY=5;
        }
        else if(!handler.isUp()){
            volY=0;
        }

        if(handler.isRight()){
            volX=5;
        }
        else if(!handler.isLeft()){
            volX=0;
        }

        if(handler.isLeft()){
            volX=-5;
        }
        else if(!handler.isRight()){
            volX=0;
        }

        if(hp<=0){
            System.exit(1);
        }
        

    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.blue);
        g.fillRect(x, y, 32, 48);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x,y,32,48);
    }


    private void collision() {
        for(int i=0;i<handler.objects.size();i++){
            GameObject tempObject=handler.objects.get(i);
            if(tempObject.getId()==ID.Wall){
                if(getBounds().intersects(tempObject.getBounds())){
                    x+=volX*-1;
                    y+=volY*-1;
                }
            }

            if(tempObject.getId()==ID.NPC){
                if(getBounds().intersects(tempObject.getBounds())){
                    x+=volX*-1;
                    y+=volY*-1;
                }
            }

            if(tempObject.getId()==ID.Enemy){
                if(getBounds().intersects(tempObject.getBounds())){
                    hp--;
                }
            }

            if(tempObject.getId()==ID.MpPotion && mp<=40){
                if(getBounds().intersects(tempObject.getBounds())){
                    mp+=10;
                    handler.removeObject(tempObject);
                }
            }

            if(tempObject.getId()==ID.Exit){
                if(getBounds().intersects(tempObject.getBounds())){
                   System.out.println("LEVEL COMPLETE");
                   System.exit(0);
                }
            }
        
        }
    }

    private void checkLevelUp(){
        int toNextLevel=10;

        if(exp>=toNextLevel){
            toNextLevel=toNextLevel*=2.5;
            level++;
            exp=0;
        }
    }

    
}