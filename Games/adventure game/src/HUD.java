import java.awt.*;

public class HUD {

    public HUD(Graphics g,int hp,int mp,int exp){
     //HUD rendering
     hpBar(g, hp);
     mpBar(g, mp);
     expBar(g, exp);
    }

    //render HP bar to screen
    private void hpBar(Graphics g,int hp){
     g.setColor(Color.gray);
     g.fillRect(5, 5, 200, 16);
     g.setColor(Color.green);
     g.fillRect(5, 5, hp*2, 16);
     g.setColor(Color.white);
     g.drawRect(5, 5, hp*2, 16);
    }

    //render MP bar to screen

    private void mpBar(Graphics g,int mp){
        g.setColor(Color.gray);
        g.fillRect(5, 25, 100, 8);
        g.setColor(new Color(3, 152, 252));
        g.fillRect(5, 25, mp*2, 8);
        g.setColor(Color.white);
        g.drawRect(5, 25, mp*2, 8);
           
    }

    private void expBar(Graphics g,int exp){
        g.setColor(Color.gray);
        g.fillRect(5, 35, 100, 4);
        g.setColor(Color.orange);
        g.fillRect(5, 35, exp*2, 4);
        g.setColor(Color.white);
        g.drawRect(5, 35, exp*2, 4);
    }


}