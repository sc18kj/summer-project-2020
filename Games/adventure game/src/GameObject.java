import java.awt.*;

//efectily the bones of each game object
public abstract class GameObject {
    
    protected int x,y;
    protected float volX=0 ,volY=0;
    protected ID id;
    

    public GameObject(int x, int y,ID id){
        this.x=x;
        this.y=y;
        this.id=id;
    }
    public abstract void tick(); //every object needs to update
    public abstract void render(Graphics g);//every object needs to render itself
    public abstract Rectangle getBounds();//everything object is a squre and soem can be collied with


    public int getX() {
        return this.x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public float getVolX() {
        return this.volX;
    }

    public void setVolX(float volX) {
        this.volX = volX;
    }

    public float getVolY() {
        return this.volY;
    }

    public void setVolY(float volY) {
        this.volY = volY;
    }


    public ID getId() {
        return this.id;
    }

    public void setId(ID id) {
        this.id = id;
    }


}